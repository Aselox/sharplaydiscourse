require_dependency 'discourse'

module Sharplay
	class ApplicationController < ActionController::Base
		include CurrentUser
	end
end
