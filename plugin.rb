# name: sharplay
# about: sharplaynetwork plugin for discourse
# version: 0.1
# authors: Boris D'Amato

::SHARPLAY_HOST = Rails.env.production? ? "dev.sharplay.net" : "sharplay.net"
::SHARPLAY_DISCOURSE = Rails.env.production? ? "l.discourse" : "discuss.sharplay.net"

module ::Sharplay
  class Engine < ::Rails::Engine
    engine_name "sharplay"
    isolate_namespace Sharplay
  end
end

Rails.configuration.assets.precompile += ['bootstrap.css']

after_initialize do

  #load File.expand_path("../app/jobs/blog_update_twitter.rb", __FILE__)
  #load File.expand_path("../app/jobs/blog_update_stackoverflow.rb", __FILE__)

  class SharplayConstraint
    def matches?(request)
      request.host == SHARPLAY_HOST
    end
  end

  Discourse::Application.routes.prepend do
    mount ::Blog::Engine, at: "/", constraints: SharplayConstraint.new
  end
end